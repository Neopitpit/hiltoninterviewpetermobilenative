class Helpers {
    static numberOfNights(startDate, untilDate) {
        if(startDate == null || untilDate == null) {
            return 0;
        }
    
        return untilDate.diff(startDate, 'days') 
    }
}
  
export default Helpers;
