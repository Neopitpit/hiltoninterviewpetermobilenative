import React from 'react';
import {
  SafeAreaView,
} from 'react-native';
import { graphql, compose } from 'react-apollo';
import * as ReservationQuery from '@hilton-app/queries/reservation.graphql';
import HotelsList from './components/hotels.component';
import * as HotelQuery from '@hilton-app/queries/hotel.graphql';
import { FAKE_USER_ID } from '@hilton-app/global/constants';
import themeStyles from '@hilton-app/global/theme.styles';

class Hotels extends React.Component {
  onReservation = (hotelId, price) => {
    const { navigation } = this.props;
    const startDate = navigation.getParam('startDate');
    const untilDate = navigation.getParam('untilDate');

    this.props.mutate({
      variables: { 
        user: FAKE_USER_ID, 
        hotel: hotelId,
        price: price,
        arrivalDate: startDate.format(),
        departureDate: untilDate.format(),
       }
    })
    .then(({ data }) => {
      navigation.navigate('Congratulations');
    }).catch((error) => {
      console.log('There was an error sending the query', error);
    });
  }

  render() {
    const { hotels, navigation } = this.props;

    return (
      <SafeAreaView style={themeStyles.safeAreaView}>
        <HotelsList 
          data={hotels.hotels}
          onReservation={this.onReservation}
          navigation={navigation} />
      </SafeAreaView>
    );
  }
}

const hotelsList = graphql(HotelQuery.hotels, {
  name: 'hotels',
});

export default compose(hotelsList, graphql(ReservationQuery.createReservation))(Hotels);
