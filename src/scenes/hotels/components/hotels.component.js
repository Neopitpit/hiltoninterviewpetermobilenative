import React from 'react';
import {
  FlatList,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import PropTypes from 'prop-types';
import { URI } from '@hilton-app/global/constants';
import HotelCard from './hotelCard.component';

class Hotels extends React.Component {
    static propTypes = {
        onReservation: PropTypes.func,
        data: PropTypes.array,
    };
    
    static defaultProps = {
        onReservation: null,
        data: [],
    };

    renderItem = (row) => {
        const { onReservation } = this.props;
        const { item } = row;
        const {
            _id,
            name,
            imageMain,
            price
        } = item;

        const imageURL = URI + imageMain.url;

        return (
            <TouchableOpacity key={_id} onPress={() => onReservation(_id, price)}>
                <HotelCard
                    name={name}
                    price={price}
                    imageURL={imageURL}
                />
            </TouchableOpacity>
        );
      }

    render() {
      const { data } = this.props;

      return (
        <FlatList
            data={data}
            renderItem={this.renderItem}
            style={styles.list}
            keyExtractor={item => item._id}
            extraData={data}
        /> 
      );
    }
}

export default Hotels;

const styles = StyleSheet.create({
    list: {
        flex: 1, 
        backgroundColor: 'lightgray',
    },
  }
);
