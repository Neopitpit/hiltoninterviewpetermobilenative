import React from 'react';
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
} from 'react-native';
import PropTypes from 'prop-types';
import HotelMainImage from '@hilton-app/components/hotelMainImage.component';
import theme from '@hilton-app/global/theme';

class HotelCard extends React.Component {
    static propTypes = {
        name: PropTypes.string,
        imageURL: PropTypes.string,
        price: PropTypes.number,
    };
    
    static defaultProps = {
        name: '',
        imageURL: null,
        price: null,
    };

    render() {
        const {
            name,
            imageURL,
            price
        } = this.props;
        const { width } = Dimensions.get('window');

      return (
        <View style={[styles.container, { width: width, }]}>
            <HotelMainImage imageURL={imageURL} />
            <View>
                <Text 
                    style={styles.name}
                    ellipsizeMode='tail'
                    numberOfLines={1}
                >{name}</Text>
                <Text style={styles.priceContainer}>
                    <Text style={styles.price}>{price} $ US</Text>
                    <Text style={styles.pricePerNight}> per night</Text>
                </Text>
            </View>
        </View>
      );
    }
}

export default HotelCard;

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        height: 335,
        backgroundColor: '#FFF',
        marginBottom:10,
    },
    name: {
        fontSize: 20,
        fontWeight:'bold',
        color: theme.inverseTextColor,
        paddingTop: 10,
        paddingLeft: 10,
        paddingRight: 10
    },
    priceContainer: {
        padding: 10,
    },
    price: {
        fontSize: 24, 
        fontWeight:'bold',
        color: theme.inverseTextColor,
        padding: 10,
    },
    pricePerNight: {
        fontSize: 12,
        color: 'gray',
        paddingLeft: 10,
    },
  }
);
