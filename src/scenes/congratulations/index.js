import React from 'react';
import {
  SafeAreaView,
  View,
  Text,
  StyleSheet,
} from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';
import HiltonButton from '@hilton-app/components/button.component';
import theme from '@hilton-app/global/theme';
import themeStyles from '@hilton-app/global/theme.styles';

class Congratulations extends React.Component {
  resetStack = () => {
    const { navigation } = this.props;
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'Searching' })],
    });

    // Reset the stack of the searching hotels stack
    navigation.dispatch(resetAction);
    // Go to the reservation screen in ortder to see the new reservation in the list
    navigation.navigate('Reservations');
  }

  render() {
    return (
      <SafeAreaView style={themeStyles.safeAreaView}>
        <View style={styles.container}>
          <Text style={styles.congratulationText}>Congratulations</Text>
          <Text style={styles.congratulationText}>hotel booked!</Text>
          <HiltonButton text={'Done'} onPress={this.resetStack}></HiltonButton>
        </View>
      </SafeAreaView>
    );
  }
}

export default Congratulations;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: theme.backgroundColorPrimary,
    alignItems: 'center',
    justifyContent: 'center',
  },
  congratulationText: {
    fontSize: 28,
    color: theme.brandThird,
  },
});
