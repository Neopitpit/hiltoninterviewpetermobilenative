import React from 'react';
import {
  Image,
  Dimensions,
} from 'react-native';
import PropTypes from 'prop-types';

class HotelMainImage extends React.Component {
  static propTypes = {
    imageURL: PropTypes.string,
  };

  static defaultProps = {
    imageURL: null,
  };

  render() {
      const { imageURL } = this.props;
      const { width } = Dimensions.get('window');

    return (
          <Image source={{uri: imageURL}} style={{width: width, height: 250}} resizeMode='stretch' />
    );
  }
}

export default HotelMainImage;
