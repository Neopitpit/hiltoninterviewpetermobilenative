import gql from 'graphql-tag';

export const hotel = gql`
  query Hotel($id: ID!) {
    hotel(id: $id) {
      _id
      name
      description
      price
      imageMain {
        url
      }
    }
  }
`;

export const hotels = gql`
  query Hotels {
    hotels {
      _id
      name
      description
    price
      imageMain {
        url
      }
    }
  }
  `;
  