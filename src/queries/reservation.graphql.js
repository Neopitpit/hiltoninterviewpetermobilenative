import gql from 'graphql-tag';

export const reservations = gql`
  query Reservations {
    reservations(sort: "arrivalDate:dsc") {
      _id
      user {
        _id
        email
      }
      hotel {
        _id
        name
        price
        imageMain {
          url
        }
      }
      arrivalDate
      departureDate
      price
    }
  }
`;

export const createReservation = gql`
  mutation CreateReservation ($user: ID!, $hotel: ID!, $price: Float, $arrivalDate: DateTime, $departureDate: DateTime){
    createReservation(input: {
      data: {
        user: $user
        hotel: $hotel
        price: $price
        arrivalDate: $arrivalDate
        departureDate: $departureDate
      }
    }) {
      reservation {
        _id
        user {
          _id
          username
          email
        }
        hotel {
          _id
          name
        }
        arrivalDate
        departureDate
        price
        updatedAt
        createdAt
      }
    }
  }
`;
