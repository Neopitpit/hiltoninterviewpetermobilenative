import { Platform, Dimensions } from 'react-native';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
const platform = Platform.OS;
const platformStyle = undefined;

export default {
  platformStyle,
  platform,

  width: deviceWidth,
  height: deviceHeight,

  // Color
  brandPrimary: '#002C51',
  brandSecondary: '#425c85',
  brandThird: '#2ECC71', 
  backgroundColorPrimary: '#FFF',
  backgroundColorSecondary: '#F2F3F4',

  // Navigation
  navPrimary: '#002C51',
  navTintColor: '#FFF',

  navTabActive: '#2f5086',
  navTabInactive: '#000',

  // Buttom
  buttonPrimary: '#002C51',
  buttonPrimaryTextColor: '#FFF',
  buttonPrimaryTextFontSize: 18,

  // Font
  fontSizeBase: 13,

  // Icon
  iconFamily: 'Ionicons',
  iconFontSize: (platform === 'ios') ? 30 : 28,

  // Text
  textColor: '#FFF',
  inverseTextColor: '#000',
};
