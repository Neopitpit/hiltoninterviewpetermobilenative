export const GRAPHQL_ENDPOINT = 'https://hilton-interview-backend.herokuapp.com/graphql';
export const URI = 'https://hilton-interview-backend.herokuapp.com';
export const FAKE_USER_ID = '5c441ffdcf076e00178d4219';
export const DATE_FORMAT = 'Do MMM';